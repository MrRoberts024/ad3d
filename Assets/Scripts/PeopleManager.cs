﻿using UnityEngine;
using System.Collections;

public class PeopleManager : MonoBehaviour {

	public People[] peoplePrefabs;

	private GameManager gameManager;
	private People[] people;

	// Use this for initialization
	void Start () {
		gameManager = FindObjectOfType<GameManager> ();

		// initialize people pool
		people = new People[peoplePrefabs.Length];
		People person;
		for (int count = 0; count < people.Length; count++) {
			person = (People) Instantiate (peoplePrefabs [count], new Vector3 (0f, 0f, -100f), peoplePrefabs [count].transform.rotation);
			person.transform.SetParent (transform);
			person.gameObject.SetActive (false);
			people [count] = person;
		}
	}

	public People GetRandomPerson() {
		int slot;
		People person;

		do {
			slot = gameManager.RandomNext (people.Length);
			person = people [slot];
		} while (person.gameObject.activeSelf);

		return person;
	}
}
