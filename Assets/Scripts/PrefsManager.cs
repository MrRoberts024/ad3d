﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

public class PrefsManager : MonoBehaviour {
	// string values and defaults for stored prefs
	const string SFX_VOLUME = "sfxvolume";
	const float DEFAULT_SFX_VOLUME = 0.5f;

	const string MUSIC = "music";
	const int DEFAULT_MUSIC = 1;

	const int DEFAULT_LEVEL = 1;
	const int MAX_LEVEL = 25;

	const string HIGH_SCORE = "high_score";
	const int DEFAULT_HIGH_SCORE = 0;
	const int MAX_SCORE = int.MaxValue;

	const string CREDITS = "credits";
	const int DEFAULT_CREDITS = 0;
	const int MAX_CREDITS = int.MaxValue;

	const string MULTIPLIER = "multiplier";
	const int DEFAULT_MULTIPLIER = 1;
	const int MAX_MULTIPLIER = 3;

	const string AA_FIRE_RATE = "aafirerate";
	const float DEFAULT_AA_FIRE_RATE = 2.5f;
	const float MIN_AA_FIRE_RATE = 0.35f;

	const string AA_BURST_RADIUS = "aaburstrate";
	const float DEFAULT_BURST_RADIUS = 3.5f;
	const float MAX_AA_BURST_RADIUS = 7f;

	const string SHELL_SPEED = "shellspeed";
	const float DEFAULT_SHELL_SPEED = 1000f;
	const float MAX_SHELL_SPEED = 1700f;

	const string TUTORIAL_COMPLETED = "tutorial";
	const int DEFAULT_TUTORIAL_COMPLETED = 0;

	const string POSTSCORE_FAILED = "postscore";
	const int DEFAULT_POSTSCORE_FAILED = 0;

	const string PLAYER_NAME = "playername";
	const string DEFAULT_NAME = "New Defender";

	public static float GetSFXVolume () {
		// check if key exists
		if (SecurePrefs.HasKey(SFX_VOLUME)) {
			//retrieve decrypted value
			float value = SecurePrefs.GetFloat(SFX_VOLUME);

			// ensure value is between 0 and 1, if not return default volume
			if (value >= 0f && value <= 1f) {
				return value;
			}
		}
		return DEFAULT_SFX_VOLUME;	
	}

	public static void SetSFXVolume (float value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetFloat(SFX_VOLUME, value);
	}

	public static void SetSFXVolume () {
		SetSFXVolume (DEFAULT_SFX_VOLUME);
	}

	public static bool GetMusic () {
		// check if key exists
		if (SecurePrefs.HasKey(MUSIC)) {
			//retrieve decrypted value
			float value = SecurePrefs.GetFloat(MUSIC);

			// ensure value is between 0 and 1, if not return default volume
			if (value == 0) {
				return false;
			}
		}
		return true;	
	}

	public static void SetMusic (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetFloat(MUSIC, value);
	}

	public static void SetMusic () {
		SetMusic (DEFAULT_MUSIC);
	}

	public static int GetHighScore () {
		// check if key exists
		if (SecurePrefs.HasKey(HIGH_SCORE)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(HIGH_SCORE);

			// ensure value is within accepted range
			if (value >= 0 && value <= MAX_SCORE) {
				return value;
			}
		}
		return DEFAULT_HIGH_SCORE;
	}

	public static void SetHighScore (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(HIGH_SCORE, value);
	}

	public static void SetHighScore ()	 {
		SetHighScore (DEFAULT_HIGH_SCORE);
	}

	public static int GetCredits () {
		// check if key exists
		if (SecurePrefs.HasKey(CREDITS)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(CREDITS);

			// ensure value is within accepted range
			if (value >= 0 && value <= MAX_CREDITS) {
				return value;
			}
		}
		return DEFAULT_CREDITS;
	}

	public static void SetCredits (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(CREDITS, value);
	}

	public static void SetCredits () {
		SetCredits (DEFAULT_CREDITS);
	}

	public static int GetMulitplier () {
		// check if key exists
		if (SecurePrefs.HasKey(MULTIPLIER)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(MULTIPLIER);

			// ensure value is within accepted range
			if (value >= 1 && value <= MAX_MULTIPLIER) {
				return value;
			}
		}
		return DEFAULT_MULTIPLIER;
	}

	public static void SetMultiplier (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(MULTIPLIER, value);
	}

	public static void SetMultiplier () {
		SetMultiplier (DEFAULT_MULTIPLIER);
	}

	public static int GetAAFireRate () {
		// check if key exists
		if (SecurePrefs.HasKey(AA_FIRE_RATE)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(AA_FIRE_RATE);

			// ensure value is between 0 and 1, if not return default volume
			if (value >= DEFAULT_LEVEL && value <= MAX_LEVEL) {
				return value;
			}
		}
		return DEFAULT_LEVEL;
	}

	public static float GetAAFireRateValue () {
		// get fire rate level
		int level = GetAAFireRate() - 1;

		// determine value
		float step = (DEFAULT_AA_FIRE_RATE - MIN_AA_FIRE_RATE) / MAX_LEVEL;
		float value = DEFAULT_AA_FIRE_RATE - (step * level);

		return value;
	}

	public static void SetAAFireRate (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(AA_FIRE_RATE, value);
	}

	public static void SetAAFireRate () {
		SetAAFireRate (DEFAULT_LEVEL);
	}

	public static int GetAABurstRadius () {
		// check if key exists
		if (SecurePrefs.HasKey(AA_BURST_RADIUS)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(AA_BURST_RADIUS);

			// ensure value is between default and max level, if not return default volume
			if (value >= DEFAULT_LEVEL && value <= MAX_LEVEL) {
				return value;
			}
		}
		return DEFAULT_LEVEL;
	}

	public static float GetAABurstRadiusValue () {
		// get fire rate level
		int level = GetAABurstRadius() - 1;

		// determine value
		float step = (MAX_AA_BURST_RADIUS - DEFAULT_BURST_RADIUS) / MAX_LEVEL;
		float value = DEFAULT_BURST_RADIUS + (step * level);

		return value;
	}

	public static void SetAABurstRadius (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(AA_BURST_RADIUS, value);
	}

	public static void SetAABurstRadius () {
		SetAABurstRadius (DEFAULT_LEVEL);
	}

	public static int GetShellSpeed () {
		// check if key exists
		if (SecurePrefs.HasKey(SHELL_SPEED)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt(SHELL_SPEED);

			// ensure value is within accepted range
			if (value >= DEFAULT_LEVEL && value <= MAX_LEVEL) {
				return value;
			}
		}
		return DEFAULT_LEVEL;
	}

	public static float GetShellSpeedValue () {
		// get fire rate level
		int level = GetShellSpeed() - 1;

		// determine value
		float step = (MAX_SHELL_SPEED - DEFAULT_SHELL_SPEED) / MAX_LEVEL;
		float value = DEFAULT_SHELL_SPEED + (step * level);

		return value;
	}

	public static void SetShellSpeed (int value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(SHELL_SPEED, value);
	}

	public static void SetShellSpeed () {
		SetShellSpeed (DEFAULT_LEVEL);
	}

	public static bool isTutorialCompleted () {
		// check if key exists
		if (SecurePrefs.HasKey (TUTORIAL_COMPLETED)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt (TUTORIAL_COMPLETED);

			// if tutorial has been completed return true
			if (value == 1) {
				return true;
			} 
		} 
		// tutorial not completed or playerprefs not found
		return false;
	}

	public static void SetTutorialCompleted () {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(TUTORIAL_COMPLETED, 1);
	}

	public static void ResetTutorialCompleted () {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(TUTORIAL_COMPLETED, 0);
	}

	public static string GetPlayerName () {
		// check if key exists
		if (SecurePrefs.HasKey(PLAYER_NAME)) {
			//retrieve decrypted value
			string value = SecurePrefs.GetString(PLAYER_NAME);
			Debug.Log ("Stored name: " + value);
			// ensure value is no longer than 20 characters
			if (value.Length <= 20) {
				return value;
			}
		}
		return DEFAULT_NAME;	
	}

	public static void SetPlayerName (string value) {
		// call secureprefs to store encrypted values
		SecurePrefs.SetString (PLAYER_NAME, value);
		Debug.Log ("Name saved: " + value);
	}

	public static void SetPlayerName () {
		// call setplayername with default player name as value passed
		SetPlayerName (DEFAULT_NAME);
	}

	public static bool PostScoreFailed () {
		// check if key exists
		if (SecurePrefs.HasKey (POSTSCORE_FAILED)) {
			//retrieve decrypted value
			int value = SecurePrefs.GetInt (POSTSCORE_FAILED);

			// if postscore failed is 1 return true
			if (value == 1) {
				return true;
			} 
		} 
		// postscore not failed or playerprefs not found
		return false;
	}

	public static void SetPostScoreFailed () {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(POSTSCORE_FAILED, 1);
	}

	public static void ResetPostScoreFailed () {
		// call secureprefs to store encrypted values
		SecurePrefs.SetInt(POSTSCORE_FAILED, 0);
	}
}
