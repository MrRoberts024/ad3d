﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsManager : MonoBehaviour {

	public Slider sfxSlider;
	public Slider musicSlider;

	public GameObject credits;

	// Use this for initialization
	void Start () {
		sfxSlider.value = PrefsManager.GetSFXVolume ();

		if (PrefsManager.GetMusic ()) {
			musicSlider.value = 1;
		} else {
			musicSlider.value = 0;
		}
	}

	// Save current values
	public void Save() {
		PrefsManager.SetSFXVolume (sfxSlider.value);
		PrefsManager.SetMusic ((int) musicSlider.value);
	}

	// Update game audio
	public void SFXVolumeChange() {
		AudioListener.volume = sfxSlider.value;
	}

	// Update game music on/off
	public void MusicChange() {
		if (musicSlider.value == 0) {
			SoundManager.instance.musicSource.mute = true;
		} else {
			SoundManager.instance.musicSource.mute = false;
		}

	}

	// Reset tutorial
	public void ResetTutorial() {
		PrefsManager.ResetTutorialCompleted ();
	}

	// Toggle Credits
	public void ToggleCredts() {
		if (credits.activeSelf) {
			credits.SetActive (false);
		} else {
			credits.SetActive (true);
		}
	}

	// reset stored prefs for testing
	public void ResetPrefs() {
		PlayerPrefs.DeleteAll ();
		PrefsManager.SetCredits ();
		PrefsManager.SetHighScore ();
		PrefsManager.SetAAFireRate ();
		PrefsManager.SetAABurstRadius ();
		PrefsManager.SetShellSpeed ();
		PrefsManager.SetPlayerName ();
		PrefsManager.ResetTutorialCompleted ();
	}
}
