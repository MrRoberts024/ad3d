﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject mortarPrefab, missilePrefab, bombPrefab, heliPrefab;
	public GameObject enemyParent;

	public float mortarFrequency;
	public float missileFrequency;
	public float bombFrequency;
	public float heliFrequency;
	public float minDifficultyDeltaTime;
	public float maxDifficultyDeltaTime;

	// random number generator
	private System.Random random = new System.Random ();

	// enemy object pools
	private int poolSize = 20;
	private Vector3 holdingLocation = new Vector3 (40f, 80f, 0f);
	private GameObject[] mortarPool = new GameObject[20];
	private GameObject[] missilePool = new GameObject[20];
	private GameObject[] bombPool = new GameObject[20];
	private GameObject heli;

	// variables to track last time of enemy spawn
	private float mortarLastTime;
	private float missileLastTime;
	private float bombLastTime;
	private float heliLastTime;

	// Use this for initialization
	void Start () {
		// instantiate pool objects
		GameObject tempObj;
		for (int count = 0; count < poolSize; count++) {
			tempObj = Instantiate(missilePrefab, holdingLocation, missilePrefab.transform.rotation) as GameObject;
			tempObj.transform.parent = enemyParent.transform;
			tempObj.GetComponent<Enemy> ().SetHoldingPosition (holdingLocation);
			tempObj.SetActive (false);
			missilePool [count] = tempObj;

			tempObj = Instantiate(mortarPrefab, holdingLocation, mortarPrefab.transform.rotation) as GameObject;
			tempObj.transform.parent = enemyParent.transform;
			tempObj.GetComponent<Enemy> ().SetHoldingPosition (holdingLocation);
			tempObj.SetActive (false);
			mortarPool [count] = tempObj;

			tempObj = Instantiate(bombPrefab, holdingLocation, bombPrefab.transform.rotation) as GameObject;
			tempObj.transform.parent = enemyParent.transform;
			tempObj.GetComponent<Enemy> ().SetHoldingPosition (holdingLocation);
			tempObj.SetActive (false);
			bombPool [count] = tempObj;
		}

		// instantiate heli
		heli = Instantiate(heliPrefab, holdingLocation, heliPrefab.transform.rotation) as GameObject;
		heli.transform.parent = enemyParent.transform;
		heli.GetComponent<Heli> ().SetHoldingPosition (holdingLocation);
		heli.SetActive (false);

		// set last launch times to current time
		mortarLastTime = Time.time;
		missileLastTime = Time.time;
		bombLastTime = Time.time;
		heliLastTime = Time.time;
		/*
		// start difficuty increasing coroutine
		StartCoroutine(IncreaseDifficulty());
		*/
	}

	void Update () {

		// if requisite time has elapsed spawn a new enemy
		if ((Time.time - mortarLastTime) > mortarFrequency) {
			SpawnMortar ();
			mortarLastTime = Time.time;
			if (mortarFrequency > 0.7f) mortarFrequency -= 0.075f;
		}
		if ((Time.time - missileLastTime) > missileFrequency) {
			SpawnMissile ();
			missileLastTime = Time.time;
			if (missileFrequency > 1f) missileFrequency -= 0.25f;
		}
		if ((Time.time - bombLastTime) > bombFrequency) {
			SpawnBomb ();
			bombLastTime = Time.time;
			if (bombFrequency > 0.7f) bombFrequency -= 0.15f;
		}
		if ((Time.time - heliLastTime) > heliFrequency) {
			SpawnHeli ();
			heliLastTime = Time.time;
			if (heliFrequency > 10f) heliFrequency -= 5f;
		}
	}

	/*
	IEnumerator IncreaseDifficulty () {
		bool firstLoop = true;
	
		while (true) {
			// increase difficulty only if it isn't the first time through the loop
			if (firstLoop) {
				firstLoop = false;
			} else {
				if (mortarFrequency > 0.5f) mortarFrequency -= 0.5f;
				if (missileFrequency > 1f) missileFrequency -= 1f;
				if (bombFrequency > 1f) bombFrequency -= 1f;
			}

			yield return new WaitForSeconds (RandomFloat(minDifficultyDeltaTime, maxDifficultyDeltaTime));
		}
	}
    */

	void SpawnMortar() {
		// create random x position for spawn
		float yCoord = 48f;
		float xCoord;
		float zCoord = mortarPrefab.transform.position.z;
		Vector3 location = new Vector3();

		// find next unused mortar prefab in pool
		for (int count = 0; count < poolSize; count++) {
			if (mortarPool [count] != null) {
				if (!mortarPool [count].activeSelf) {
					GameObject obj = mortarPool [count];
					// ensure object isn't spawned on top of another
					for (int tries = 0; tries < 5; tries++) {
						xCoord = RandomFloat(23f, 95f);
						location = new Vector3 (xCoord, yCoord, zCoord);
						if (IsSpawnClear (location, obj.name)) {
							break;
						}
					}
					obj.transform.position = location;
					obj.transform.rotation = mortarPrefab.transform.rotation;
					obj.GetComponent<Rigidbody>().velocity = new Vector3(-3f,0f,0f);
					obj.SetActive (true);
					break;
				}
			}
		}
	}

	void SpawnMissile() {
		// create random x position for spawn
		float yCoord = 48f;
		float xCoord;
		float zCoord = missilePrefab.transform.position.z;
		Vector3 location = new Vector3();

		// find next unused misile prefab in pool
		for (int count = 0; count < poolSize; count++) {
			if (missilePool [count] != null) {
				if (!missilePool [count].activeSelf) {
					GameObject obj = missilePool [count];
					// ensure object isn't spawned on top of another
					for (int tries = 0; tries < 5; tries++) {
						xCoord = RandomFloat(51f, 107f);
						location = new Vector3 (xCoord, yCoord, zCoord);
						if (IsSpawnClear (location, obj.name)) {
							break;
						}
					}
					obj.transform.position = location;
					obj.transform.rotation = missilePrefab.transform.rotation;
					obj.GetComponent<Rigidbody>().velocity = new Vector3(-8f,0f,0f);
					obj.SetActive (true);
					break;
				}
			}
		}
	}

	void SpawnBomb() {
		// create random x position for spawn
		float yCoord = 48f;
		float xCoord;
		float zCoord = bombPrefab.transform.position.z;
		Vector3 location = new Vector3();

		// find next unused misile prefab in pool
		for (int count = 0; count < poolSize; count++) {
			if (bombPool [count] != null) {
				if (!bombPool [count].activeSelf) {
					GameObject obj = bombPool [count];
					// ensure object isn't spawned on top of another
					for (int tries = 0; tries < 5; tries++) {
						xCoord = RandomFloat(35f, 101f);
						location = new Vector3 (xCoord, yCoord, zCoord);
						if (IsSpawnClear (location, obj.name)) {
							break;
						}
					}
					obj.transform.position = location;
					obj.transform.rotation = bombPrefab.transform.rotation;
					obj.GetComponent<Rigidbody>().velocity = new Vector3(-5f,0f,0f);
					obj.SetActive (true);
					break;
				}
			}
		}
	}

	void SpawnHeli() {
		if (!heli.activeSelf) {
			heli.transform.rotation = heliPrefab.transform.rotation;
			heli.SetActive (true);
		}
	}

	bool IsSpawnClear (Vector3 location, string name) {
		// create layer mask for enemyProjectile layer
		int layerMask = 1 << 10;

		// draw physics sphere to determine what overlaps potential spawn area
		Collider[] hitColliders = Physics.OverlapSphere(location, 2.0f, layerMask);

		// if anything found determine if it is same type as player
		for (int count = 0; count < hitColliders.Length; count++) {
			if (hitColliders [count].gameObject.name == name) {
				Debug.Log ("spawn not clear");
				return false;
			}
		}
		return true;
	}

	private float RandomFloat ( float min, float max) {
		double rnd = random.NextDouble ();
		return (float)(rnd * (max - min) + min);
	}
}
