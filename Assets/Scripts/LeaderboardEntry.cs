﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeaderboardEntry : MonoBehaviour {
	//creating the public variables that we can pass the GameSparks information to
	public string rankString, usernameString, scoreString;

	public Text rank, username, score;

	void Start()
	{
		rank.text = rankString;
		username.text = usernameString;
		score.text = scoreString;
	}
}


