﻿using UnityEngine;
using System.Collections;

public class Shell : MonoBehaviour {

	public AudioClip audioClip;
	public GameObject explosionEffect;

	private float aaBlastRadius;
	private Vector3 targetPoint;
	private Vector3 holdingPoint;
	private enum LaunchDirection {Right, Left, Up};
	private LaunchDirection launchDirection = LaunchDirection.Right;
	private GameManager gameManager;

	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
		aaBlastRadius = PrefsManager.GetAABurstRadiusValue ();
	}

	// Update is called once per frame
	void Update () {
		// Determine if shell has travelled to go boom point
		if (launchDirection == LaunchDirection.Right) {
			if (transform.position.x >= targetPoint.x) {
				GoBoom ();
			}
		} else if (launchDirection == LaunchDirection.Left) {
			if (transform.position.x <= targetPoint.x) {
				GoBoom ();
			}
		} else {
			if (transform.position.y >= targetPoint.y) {
				GoBoom ();
			}
		}
	}

	public void SetTargetPoint (Vector3 point) {
		targetPoint = point;
		if (targetPoint.x > transform.position.x) {
			launchDirection = LaunchDirection.Right;
		} else if (targetPoint.x < transform.position.x) {
			launchDirection = LaunchDirection.Left;
		} else {
			launchDirection = LaunchDirection.Up;
		}
	}

	public void SetHoldingPoint (Vector3 point) {
		holdingPoint = point;
	}

	// Go Boom!
	void GoBoom () {
		// create layer mask
		int layerMask = 1 << 10;

		// draw physics sphere to determine what has been hit
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, aaBlastRadius, layerMask);

		// if anything hit score hit
		if (hitColliders.Length >= 1) {gameManager.ShotHit();}
	
		// if more than one enemy hit with a single shell give bonus points
		if (hitColliders.Length > 1) {
			gameManager.BonusHit (hitColliders.Length);
		}

		// iterate through hit objects and call GoBoom method
		int i = 0;
		while (i < hitColliders.Length) {
			hitColliders [i].SendMessage ("GoBoom");
			Debug.Log (hitColliders [i].name);
			i++;
		}

		// spawn particle system
		Instantiate(explosionEffect, transform.position, explosionEffect.transform.rotation);

		// play explode audio
		SoundManager.instance.PlaySingle (audioClip);

		// Deactivate this object, nullify velocity, move to holding point
		gameObject.SetActive(false);
		gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		gameObject.transform.position = holdingPoint;
	}
		
	// Collision with another object
	void OnCollisionEnter (Collision col) {
		GoBoom();
	}
}
