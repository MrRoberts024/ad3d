﻿using UnityEngine;
using System.Collections;

public class SplashTransition : MonoBehaviour {

	public LevelManager levelManager;
	public float duration;

	// Use this for initialization
	void Start () {
		Invoke ("Transition", duration);
	}

	void Transition() {
		levelManager.GetComponent<LevelManager> ().LoadNextLevel ();
	}
}
