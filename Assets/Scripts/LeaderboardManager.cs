﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Requests;

//the prefab we are going to instantiate 
public class LeaderboardManager : MonoBehaviour
{

	public GameObject leaderboardEntryPrefab;
	public GameObject leaderboardContent;
	public ScrollRect leaderboard;
	public InputField playerName;

	public GameManager gameManager;

	void Start () {
		gameManager = FindObjectOfType<GameManager> ();
	}

	void OnEnable () {
		RefreshLeaderboard ();

		// update player name from prefs
		if (playerName != null) {
			playerName.text = PrefsManager.GetPlayerName ();
		}
	}

	void OnDisable () {
		DestroyLeaderboardEntries ();
	}

	public void RefreshLeaderboard()
	{
		// first check if a score didn't post correctly
		if (PrefsManager.PostScoreFailed ()) {
			// attempt to post current high score again
			new GameSparks.Api.Requests.LogEventRequest_postScore ().Set_score (PrefsManager.GetHighScore ()).Send ((response) => {
				if (response.HasErrors) {
					Debug.Log ("Failed reposting score");
				} else {
					Debug.Log ("Success reposting score");
					PrefsManager.ResetPostScoreFailed ();
				}
			});
		} else {
			Debug.Log ("Skipped reposting score");
		}

		GetLeaderboardEntries ();
	}

	void GetLeaderboardEntries ()
	{
		//pulls in the Leaderboard information from Gamesparks
		new AroundMeLeaderboardRequest_allTimeLeaderboard ().SetEntryCount (8).SetIncludeFirst(10).Send (response =>  {
			//what we will do with the information given by GameSparks
			foreach (var entry in response.Data) {
				GameObject go = Instantiate (leaderboardEntryPrefab) as GameObject;
				go.GetComponent<LeaderboardEntry> ().rankString = entry.Rank.ToString ();
				go.GetComponent<LeaderboardEntry> ().usernameString = entry.UserName.ToString ();
				//the score string has to be added as a number value 
				go.GetComponent<LeaderboardEntry> ().scoreString = entry.GetNumberValue ("score").ToString ();
				// if leaderboard entry is this users change text color to red
				if (gameManager.getUserId () == entry.UserId) {
					go.GetComponent<LeaderboardEntry> ().rank.color = Color.green;
					go.GetComponent<LeaderboardEntry> ().username.color = Color.green;
					go.GetComponent<LeaderboardEntry> ().score.color = Color.green;
				}
				//adds the gameobject to the list of entries
				go.transform.SetParent (leaderboardContent.transform);
				go.transform.localScale = Vector3.one;
			}
		});
	}

	public void UpdateName() 
	{
		// check if playername needs to be updated, wrapped in null check because optionsmanager used in-game pause menu
		if (playerName != null) {
			if (playerName.text != PrefsManager.GetPlayerName()) {
				PrefsManager.SetPlayerName (playerName.text);
				// send display name change to gamesparks
				new GameSparks.Api.Requests.ChangeUserDetailsRequest().SetDisplayName(playerName.text).Send((response) => {
					if (!response.HasErrors) {
						Debug.Log("Display name updated");
						DestroyLeaderboardEntries ();
						GetLeaderboardEntries ();
					} else {
						Debug.Log("Error updating display name");
					}
				});
			}
		}

	}

	void DestroyLeaderboardEntries ()
	{
		// destroy leaderboard entries
		for (int i = 0; i < leaderboardContent.transform.childCount; i++) {
			GameObject child = leaderboardContent.transform.GetChild (i).gameObject;
			if (child != null) {
				Destroy (child);
			}
		}
	}
}