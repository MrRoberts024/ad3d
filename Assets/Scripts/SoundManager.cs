﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	
	public AudioSource sfxSource;
	public AudioSource musicSource;

	public AudioClip[] musicTracks;

	public static SoundManager instance = null;  

	// variables for sound pitch variation
	public float lowPitch = .95f;
	public float highPitch = 1.05f;

	void Awake () {
		// singleton soundmanager
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
		} else if (instance != this) {
			Destroy (gameObject);
		}

		// If music is on start playing it
		if (!PrefsManager.GetMusic()) {
			musicSource.mute = true;
		}

		// start the coroutine that managers the music track changes
		StartCoroutine(ChangeMusicTrack(musicSource.clip.length));
	}

	// Play single sound clip, pass max volume to overload method
	public void PlaySingle(AudioClip clip) {
		// pass clip to overloaded method with max volume
		PlaySingle(clip, 1.0f);
	}

	// Play sound clip with specific volume
	public void PlaySingle(AudioClip clip, float vol) {
		// set passed in clip to audio clip for sfx source and play it
		sfxSource.PlayOneShot (clip, vol);
	}

	// Pick a new music track and play it
	private IEnumerator ChangeMusicTrack(float trackLength) {
		// wait until amount of time remaining in track has elapsed
		trackLength += 1f; // add a second of padding to track changes
		yield return StartCoroutine(CoRoutineUtil.WaitForRealSeconds (trackLength));

		// ensure same track isn't played twice
		int rand;
		do {
			rand = Random.Range (0, musicTracks.Length);
		} while (musicSource.clip.name == musicTracks [rand].name) ;

		// swap audio clip for new song
		musicSource.Stop ();
		musicSource.clip = musicTracks [rand];
		musicSource.Play ();

		// call this method again with delay equal to new track length
		StartCoroutine(ChangeMusicTrack(musicSource.clip.length));
	}
}
