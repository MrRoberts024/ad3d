﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Launcher : MonoBehaviour {

	public GameObject aaShell;
	public GameObject shellParent;
	public GameObject rubble;
	public GameObject explosion;
	public AudioClip explosionSound;
	public AudioClip shotSound;

	private GameManager gameManager;
	private float fireInterval;
	private float launchVelocity;
	//private float lastFire;
	private float verticalShellOffset = 4f;

	// missile rails
	int activeRails = 2;
	private GameObject[] missileRails;

	// shell pool
	private int poolSize = 10;
	private Vector3 holdingLocation = new Vector3 (-20f, -20f, 0f);
	private GameObject[] shellPool = new GameObject[10];

	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
		fireInterval = PrefsManager.GetAAFireRateValue ();
		launchVelocity = PrefsManager.GetShellSpeedValue ();
		//lastFire = Time.time;

		// get missile references
		missileRails = new GameObject[activeRails];
		missileRails [0] = GameObject.Find ("missile_04");
		missileRails [1] = GameObject.Find ("missile_03");

		// build shell pool
		GameObject tempShell;
		for (int count = 0; count < poolSize; count++) {
			tempShell = Instantiate(aaShell, holdingLocation, aaShell.transform.rotation) as GameObject;
			tempShell.transform.parent = shellParent.transform;
			tempShell.transform.position = holdingLocation;
			tempShell.SetActive (false);
			shellPool [count] = tempShell;
		}
	}

	IEnumerator ReloadMissile (GameObject rail) {
		yield return new WaitForSeconds (fireInterval);
		rail.SetActive (true);
	}

	private void FireDetermination (Vector3 touchWorldPoint, Vector3 touchPosition) {
		// determine if missile fire or civilian collect
		if (touchWorldPoint.y >= 4.5) {
			// determine if a missile is ready to fire
			for (int count = 0; count < missileRails.Length; count++) {
				if (missileRails [count].activeSelf) {
					// rail is ready
					// fire missile if above launcher, otherwise check if civilian touched
					Fire (touchWorldPoint);

					// toggle inactive
					missileRails[count].SetActive(false);

					// invoke activate method after fire interval
					StartCoroutine(ReloadMissile(missileRails[count]));

					// break from loop since availalbe missile was found
					break;
				}
			}
		} else {
			CheckCivilians(touchPosition);
		}
	}
	
	// Update is called once per frame
	void Update () {
		// determine if game not paused
		if (Time.timeScale != 0f) {

			#if UNITY_EDITOR
			// determine if mouseclick
			if (Input.GetMouseButtonDown(0)) {
				// determine if touch is over UI button
				if (!EventSystem.current.IsPointerOverGameObject ()) {
					// record position of touch
					Vector2 touchPosition = Input.mousePosition;

					// translate touch position to world position
					Vector3 screenPoint = new Vector3(touchPosition.x, touchPosition.y, Camera.main.nearClipPlane - Camera.main.transform.position.z);
					Vector3 touchWorldPoint = Camera.main.ScreenToWorldPoint(screenPoint);

					FireDetermination(touchWorldPoint, touchPosition);
				}
			}
			#endif

			#if UNITY_IOS
			// determine if any touches happened
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
				// determine if touch is over UI button
				if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) {
					// record position of touch
					Vector2 touchPosition = Input.GetTouch (0).position;

					// translate touch position to world position
					Vector3 screenPoint = new Vector3(touchPosition.x, touchPosition.y, Camera.main.nearClipPlane - Camera.main.transform.position.z);
					Vector3 touchWorldPoint = Camera.main.ScreenToWorldPoint(screenPoint);

					FireDetermination(touchWorldPoint, touchPosition);
				}
			}
			#endif

			#if UNITY_ANDROID
			// determine if any touches happened
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
				// determine if touch is over UI button
				if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) {
					// record position of touch
					Vector2 touchPosition = Input.GetTouch (0).position;

					// translate touch position to world position
					Vector3 screenPoint = new Vector3(touchPosition.x, touchPosition.y, Camera.main.nearClipPlane - Camera.main.transform.position.z);
					Vector3 touchWorldPoint = Camera.main.ScreenToWorldPoint(screenPoint);

					FireDetermination(touchWorldPoint, touchPosition);
				}
			}
			#endif
		}
	}

	void Fire (Vector3 touchWorldPoint)
	{
		// calculate launch vector, vx = launchVelocity * cos theta, vy = launchVelocity * sin theta
		float angle = Mathf.Acos ((touchWorldPoint.x - transform.position.x) / Mathf.Sqrt (Mathf.Pow ((touchWorldPoint.x - transform.position.x), 2f) + Mathf.Pow ((touchWorldPoint.y - (transform.position.y + verticalShellOffset)), 2f)));
		Vector3 launchVector = new Vector3 (launchVelocity * Mathf.Cos (angle), launchVelocity * Mathf.Sin (angle));
		// fire shell
		// find next unused shell in pool
		for (int count = 0; count < poolSize; count++) {
			if (!shellPool [count].activeSelf) {
				GameObject obj = shellPool [count];
				Vector3 holding = new Vector3 (transform.position.x, transform.position.y + verticalShellOffset, transform.position.z);
				obj.transform.position = holding;
				obj.transform.LookAt (2.0f * holding - launchVector);
				obj.SetActive (true);
				obj.GetComponent<Shell> ().SetTargetPoint (touchWorldPoint);
				obj.GetComponent<Shell> ().SetHoldingPoint (holdingLocation);
				obj.GetComponent<Rigidbody> ().AddForce (launchVector);
				break;
			}
		}

		// play fire sound
		SoundManager.instance.PlaySingle(shotSound, 0.25f);

		// record time of firing
		//lastFire = Time.time;

		// update shot fire counter
		gameManager.ShotFired ();
	}

	void CheckCivilians (Vector3 touchPosition) {
		// create layer mask
		int layerMask = 1 << 12;

		// raytrace from camera to point touched to determine if civilian was touched
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (touchPosition);
		Debug.DrawRay (ray.origin, ray.direction*75f, Color.cyan, 2f, true);
		if (Physics.Raycast(ray, out hit, 75f, layerMask)) {
			hit.transform.SendMessage ("PersonTouched");
		}
	}

	void OnCollisionEnter (Collision col) {
		// destroy missile
		Destroy (col.gameObject);

		// notify gamemanager aa destroyed
		gameManager.AADestroyed ();

		// spawn city explosin particle system
		Instantiate(explosion, transform.position + explosion.transform.position, explosion.transform.rotation);

		// play explosion sound
		SoundManager.instance.PlaySingle (explosionSound);

		// place rubble pile
		GameObject obj = (GameObject) Instantiate(rubble, transform.position, transform.rotation);
		obj.transform.SetParent (transform.parent);

		// destroy aa
		Destroy (gameObject);
	}
}
