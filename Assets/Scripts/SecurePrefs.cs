﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

public class SecurePrefs {
	// crypto values
	private const int ITERATIONS = 100;
	private const string PW = "ueAngmyReVxwu4ISpvK1";

	public static float GetFloat (string key) {
		string hashedKey = GenerateMD5(key);
		if (PlayerPrefs.HasKey (hashedKey)) {
			string encryptedValue = PlayerPrefs.GetString (hashedKey);
			string decryptedValue;
			if (Decrypt (encryptedValue, PW, out decryptedValue)) {
				return float.Parse (decryptedValue, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
			}
		}
		return 0f;
	}

	public static void SetFloat (string key, float value) {
		// generate md5 hash of key
		string hashedKey = GenerateMD5(key);

		// convert float to string for storage
		string stringValue = value.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);;

		// encrypt string
		string encryptedValue  = Encrypt(stringValue, PW);

		// store hash of key and encrypted value in playerprefs
		PlayerPrefs.SetString(hashedKey, encryptedValue);
	}

	public static int GetInt (string key) {
		string hashedKey = GenerateMD5(key);
		if (PlayerPrefs.HasKey (hashedKey)) {
			string encryptedValue = PlayerPrefs.GetString (hashedKey);
			string decryptedValue;
			if (Decrypt (encryptedValue, PW, out decryptedValue)) {
				return int.Parse (decryptedValue, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
			}
		}
		return 0;
	}

	public static void SetInt (string key, int value) {
		// generate md5 hash of key
		string hashedKey = GenerateMD5(key);

		// convert float to string for storage
		string stringValue = value.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);;

		// encrypt string
		string encryptedValue  = Encrypt(stringValue, PW);

		// store hash of key and encrypted value in playerprefs
		PlayerPrefs.SetString(hashedKey, encryptedValue);
	}

	public static string GetString (string key) {
		string hashedKey = GenerateMD5(key);
		if (PlayerPrefs.HasKey (hashedKey)) {
			string encryptedValue = PlayerPrefs.GetString (hashedKey);
			string decryptedValue;
			if (Decrypt (encryptedValue, PW, out decryptedValue)) {
				return decryptedValue;
			}
		}
		return "";
	}

	public static void SetString (string key, string value) {
		// generate md5 hash of key
		string hashedKey = GenerateMD5(key);

		// encrypt string
		string encryptedValue  = Encrypt(value, PW);

		// store hash of key and encrypted value in playerprefs
		PlayerPrefs.SetString(hashedKey, encryptedValue);
	}

	public static bool HasKey (string key) {
		// generate md5 hash of key
		string hashedKey = GenerateMD5(key);

		// check if key exists
		return PlayerPrefs.HasKey(hashedKey);  
	}


	// encrypt provided string using password
	private static string Encrypt(string plainText, string password)
	{
		// create instance of the DES crypto provider
		var des = new DESCryptoServiceProvider();

		// generate a random IV will be used a salt value for generating key
		des.GenerateIV();

		// use derive bytes to generate a key from the password and IV
		var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, des.IV, ITERATIONS);

		// generate a key from the password provided
		byte[] key = rfc2898DeriveBytes.GetBytes(8);

		// encrypt the plainText
		using (var memoryStream = new MemoryStream())
		using (var cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, des.IV), CryptoStreamMode.Write))
		{
			// write the salt first not encrypted
			memoryStream.Write(des.IV, 0, des.IV.Length);

			// convert the plain text string into a byte array
			byte[] bytes = Encoding.UTF8.GetBytes(plainText);

			// write the bytes into the crypto stream so that they are encrypted bytes
			cryptoStream.Write(bytes, 0, bytes.Length);
			cryptoStream.FlushFinalBlock();

			return Convert.ToBase64String(memoryStream.ToArray());
		}
	}

	// decrypt provided string using password
	private static bool Decrypt(string cipherText, string password, out string plainText)
	{
		try
		{   
			byte[] cipherBytes = Convert.FromBase64String(cipherText);

			using (var memoryStream = new MemoryStream(cipherBytes))
			{
				// create instance of the DES crypto provider
				var des = new DESCryptoServiceProvider();

				// get the IV
				byte[] iv = new byte[8];
				memoryStream.Read(iv, 0, iv.Length);

				// use derive bytes to generate key from password and IV
				var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, iv, ITERATIONS);

				byte[] key = rfc2898DeriveBytes.GetBytes(8);

				using (var cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, iv), CryptoStreamMode.Read))
				using (var streamReader = new StreamReader(cryptoStream))
				{
					plainText = streamReader.ReadToEnd();
					return true;
				}
			}
		}
		catch(Exception ex)
		{
			Debug.LogError (ex.ToString());
			plainText = "";
			return false;
		}
	}

	// generate MD5 hash of provided string and return the hash as a string
	private static string GenerateMD5(string text)
	{
		var md5 = MD5.Create();
		byte[] inputBytes = Encoding.UTF8.GetBytes(text);
		byte[] hash = md5.ComputeHash(inputBytes);

		// step 2, convert byte array to hex string
		var sb = new StringBuilder();
		for (int i = 0; i < hash.Length; i++)
		{
			sb.Append(hash[i].ToString("X2"));
		}
		return sb.ToString();
	}
}
