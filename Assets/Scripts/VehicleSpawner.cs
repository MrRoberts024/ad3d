﻿using UnityEngine;
using System.Collections;

public class VehicleSpawner : MonoBehaviour {

	public GameObject[] vehicles;

	// Use this for initialization
	void Start () {
		GameManager gameManager = GameObject.FindObjectOfType<GameManager> ();
		GameObject obj;
		int slot;
		// 50/50 chance of vehicle at each spot
		// if vehicle is there assign random one
		for (int count = 0; count < transform.childCount; count++) {
			if (gameManager.RandomNext(2) == 1) {
				slot = gameManager.RandomNext (vehicles.Length);
				obj = (GameObject) Instantiate (vehicles [slot], transform.GetChild (count).position, vehicles [slot].transform.rotation);
				obj.transform.SetParent (transform.GetChild (count).transform);
			}
		}
	}
}
