﻿using UnityEngine;
using System.Collections;

public class Heli : MonoBehaviour {

	public int pointValue;
	public int damage;
	public float moveRate;
	public float launchInterval;
	public GameObject miniMissilePrefab;

	private GameManager gameManager;
	private Vector3 holdingPoint;
	private Vector3 leftRotation;
	private Vector3 rightRotation;
	private bool fromLeft, reachedLaunchPoint;
	private float launchTime;

	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
		leftRotation = new Vector3 (-10f, -90f, 0f);
		rightRotation = new Vector3 (-10f, 90f, 0f);
		OnEnable ();
	}

	void OnEnable () {
		reachedLaunchPoint = false;
		if (Random.Range (0f, 1f) < 0.5f) {
			fromLeft = true;
			transform.position = new Vector3(-10,12,4);
			transform.Rotate (leftRotation);
		} else {
			fromLeft = false;
			transform.position = new Vector3(90,12,4);
			transform.Rotate (rightRotation);
		}
	}

	void Update () {
		if (!reachedLaunchPoint) {
			if (fromLeft) {
				if (transform.position.x < 15) {
					transform.position += Vector3.right * (Time.deltaTime * moveRate);
				} else {
					reachedLaunchPoint = true;
				}
			} else {
				if (transform.position.x > 65) {
					transform.position += Vector3.left * (Time.deltaTime * moveRate);
				} else {
					reachedLaunchPoint = true;
				}
			}
		} else {
			if ((Time.time - launchTime) > launchInterval) {
				LaunchMissile ();
				launchTime = Time.time;
			}
		}
	}

	void LaunchMissile () {
		GameObject missile = Instantiate (miniMissilePrefab, transform.position, miniMissilePrefab.transform.rotation) as GameObject;
		Vector3 target = new Vector3 (40f, 3f, 0f);
		missile.transform.LookAt (target);
		Vector3 dir = target - missile.transform.position;
		dir = dir.normalized;
		missile.GetComponent<Rigidbody> ().AddForce (dir * 325f);
	}

	public void SetHoldingPosition (Vector3 point) {
		holdingPoint = point;
	}

	void GoBoom () {
		// Update score
		gameManager.UpdateScore(pointValue);
		Debug.Log ("Heli go boom");
		// deactivate this enemy
		Deactivate ();
	}

	void Deactivate () {
		// Deactivate this object, move to holding position
		gameObject.SetActive (false);
		gameObject.transform.position = holdingPoint;
	}
}
