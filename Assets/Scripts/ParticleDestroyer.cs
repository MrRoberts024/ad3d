﻿using UnityEngine;
using System.Collections;

public class ParticleDestroyer : MonoBehaviour {

	private float destroyDelay = 2.5f;

	// Use this for initialization
	void Start () {
		Invoke ("Destroy", destroyDelay);
	}

	void Destroy() {
		Destroy (gameObject);
	}
}
