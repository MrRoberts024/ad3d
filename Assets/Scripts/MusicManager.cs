﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

	public AudioClip[] musicPlaylist;
	private AudioSource audioSource;

	private static GameObject thisManager;

	void Awake() {
		// singleton gamemanager
		if (thisManager == null) {
			thisManager = gameObject;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	void Start() {
		

		audioSource = GetComponent<AudioSource> ();
		audioSource.Play();
	}
}
