﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

	public GameObject pauseMenu;
	public GameObject tutorialPanel;
	public GameObject optionsPanel;

	// tutorial text references
	public GameObject tutText01;
	public GameObject tutText02;
	public GameObject tutText03;
	public GameObject tutText04;
	public GameObject tutText05;
	public GameObject tutText06;
	public GameObject tutText07;

	private GameManager gameManager;

	private int currentTutorialText = 1;
	private GameObject[] tutorialTexts;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();

		// if tutorial hasn't been displayed show it
		if (!PrefsManager.isTutorialCompleted()) {
			tutorialPanel.SetActive (true);
		}
	}

	public void PausePressed() {
		if (Time.timeScale == 0f) {
			// paused, so unpause
			Time.timeScale = 1f;
			pauseMenu.SetActive (false);
		} else {
			// unpaused, so pause
			Time.timeScale = 0f;
			pauseMenu.SetActive (true);
		}
	}

	public void ToggleOptions() {
		if (optionsPanel.activeSelf) {
			optionsPanel.SetActive (false);
		} else {
			optionsPanel.SetActive (true);
		}
	}

	public void EndGamePressed() {
		gameManager.LoadLoseScreen();
	}

	public void TutorialContinue() {
		// enable and disable tutorial texts
		switch (currentTutorialText) {
		case 1:
			tutText01.SetActive (false);
			tutText02.SetActive (true);
			break;
		case 2:
			tutText02.SetActive (false);
			tutText03.SetActive (true);
			break;
		case 3:
			tutText03.SetActive (false);
			tutText04.SetActive (true);
			break;
		case 4:
			tutText04.SetActive (false);
			tutText05.SetActive (true);
			break;
		case 5:
			tutText05.SetActive (false);
			tutText06.SetActive (true);
			break;
		case 6:
			tutText06.SetActive (false);
			tutText07.SetActive (true);
			break;
		case 7:
			tutorialPanel.SetActive (false);
			Time.timeScale = 1f;
			PrefsManager.SetTutorialCompleted ();
			break;
		default:
			break;
		}

		// incremement tutorial text
		currentTutorialText++;
	}
}
