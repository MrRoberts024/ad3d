﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoreManager : MonoBehaviour {

	public Text creditsText;

	const float COST_MULTIPLIER = 1.25f;
	const float BASE_COST = 5f;
	const int MAX_LEVEL = 25;

	private int credits, rateLevel, radiusLevel, shellLevel;
	private int rateCost, radiusCost, shellCost;

	// level texts
	public Text aaRateLevel;
	public Text aaRadiusLevel;
	public Text shellSpeedLevel;

	// cost texts
	public Text aaRateCost;
	public Text aaRadiusCost;
	public Text shellSpeedCost;

	// buy buttons
	public Button aaRateBuy;
	public Button aaRadiusBuy;
	public Button shellSpeedBuy;

	// Use this for initialization
	void OnEnable () {
		UpdateStore();
	}

	public void UpdateStore () {
		// set credits remaining text
		credits = PrefsManager.GetCredits ();
		creditsText.text = credits.ToString ();

		// set level texts
		rateLevel = PrefsManager.GetAAFireRate();
		aaRateLevel.text = rateLevel.ToString();

		radiusLevel = PrefsManager.GetAABurstRadius ();
		aaRadiusLevel.text = radiusLevel.ToString ();

		shellLevel = PrefsManager.GetShellSpeed ();
		shellSpeedLevel.text = shellLevel.ToString ();

		// set cost texts
		// first determine costs
		if (rateLevel < MAX_LEVEL) {
			rateCost = (int) (BASE_COST * Mathf.Pow (COST_MULTIPLIER, rateLevel));
			aaRateCost.text = rateCost.ToString ();
		} else {
			aaRateCost.text = "";
		}

		if (radiusLevel < MAX_LEVEL) {
			radiusCost = (int) (BASE_COST * Mathf.Pow (COST_MULTIPLIER, radiusLevel)); 
			aaRadiusCost.text = radiusCost.ToString ();
		} else {
			aaRadiusCost.text = "";
		}

		if (shellLevel < MAX_LEVEL) {
			shellCost = (int) (BASE_COST * Mathf.Pow (COST_MULTIPLIER, shellLevel));
			shellSpeedCost.text = shellCost.ToString ();
		} else {
			shellSpeedCost.text = "";
		}

		// modify buy button color/display based on level and whether there are enough credits
		if (rateLevel < MAX_LEVEL) {
			if (rateCost <= credits) {
				aaRateBuy.interactable = true;
			} else {
				aaRateBuy.interactable = false;
			}
		} else {
			aaRateBuy.interactable = false;
		}

		if (radiusLevel < MAX_LEVEL) {
			if (radiusCost <= credits) {
				aaRadiusBuy.interactable = true;
			} else {
				aaRadiusBuy.interactable = false;
			}
		} else {
			aaRadiusBuy.interactable = false;
		}

		if (shellLevel < MAX_LEVEL) {
			if (shellCost <= credits) {
				shellSpeedBuy.interactable = true;
			} else {
				shellSpeedBuy.interactable = false;
			}
		} else {
			shellSpeedBuy.interactable = false;
		}
	}

	public void BuyAARate () {
		// update credits and level
		credits -= rateCost;
		rateLevel++;

		// save to prefs
		PrefsManager.SetCredits (credits);
		PrefsManager.SetAAFireRate (rateLevel);

		// update store screen
		UpdateStore();
	}

	public void BuyAARadius () {
		// update credits and level
		credits -= radiusCost;
		radiusLevel++;

		// save to prefs
		PrefsManager.SetCredits (credits);
		PrefsManager.SetAABurstRadius (radiusLevel);

		// update store screen
		UpdateStore();
	}

	public void BuyBuilding () {
		// update credits and level
		credits -= shellCost;
		shellLevel++;

		// save to prefs
		PrefsManager.SetCredits (credits);
		PrefsManager.SetShellSpeed (shellLevel);

		// update store screen
		UpdateStore();
	}

	public void AddCredits (int c) {
		// increment credits by input variable
		credits += c;

		// save credits to prefs
		PrefsManager.SetCredits (credits);

		// update store screen
		UpdateStore();
	}
}
