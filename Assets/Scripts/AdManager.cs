﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;

public class AdManager : MonoBehaviour {

	public GameObject adButton;

	private GameManager gameManager;
	private bool adWatched = false;
	private bool adOffered = true;

	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();

		if (!Advertisement.IsReady () || !gameManager.showAdOption()) {
			adButton.SetActive (false);
			adOffered = false;
		}

		Advertisement.Show ("video");
	}

	void OnDestroy () {
		// send analytics to unity
		Dictionary<string, object> customData = new Dictionary<string, object> () {
			{ "adOffered", adOffered },
			{ "adWatched", adWatched }
		};
		AnalyticsResult analyticResult = Analytics.CustomEvent ("Ad", customData);
		Debug.Log (analyticResult);
	}

	public void ShowRewardedAd() {
		if (Advertisement.IsReady ()) {
			ShowOptions options = new ShowOptions { resultCallback = HandleShowResult };
			Advertisement.Show ("rewardedVideo", options);
		} 
	}

	public void ShowGenericAd() {
		if (Advertisement.IsReady ()) {
			Advertisement.Show ("rewardedVideo");
		} 
	}

	private void HandleShowResult(ShowResult result) {
		switch (result) {
		case ShowResult.Finished:
				// hide ad button
			adButton.SetActive (false);

				// double credits
			gameManager.AdWatched ();

			adWatched = true;

				break;
			case ShowResult.Skipped:
				Debug.Log("The ad was skipped before reaching the end.");
				break;
			case ShowResult.Failed:
				Debug.LogError("The ad failed to be shown.");
				break;
		}
	}
}
