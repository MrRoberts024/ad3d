﻿using UnityEngine;
using System.Collections;
using System;

public class Building : MonoBehaviour {

	public GameObject rubble;
	public GameObject explosion;
	public AudioClip buildingExplosionSound;

	private GameManager gameManager;
	private PeopleManager peopleManager;
	private float minPeopleSpawnTime = 10f;
	private float maxPeopleSpawnTime = 30f;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
		peopleManager = GameObject.FindObjectOfType<PeopleManager> ();
		StartCoroutine (SpawnPeople ());
	}

	// Collision
	void OnCollisionEnter (Collision col) {
		// deactivate enemy
		col.gameObject.SendMessage("Deactivate");

		// notify gamemanager city destroyed
		gameManager.CityDestroyed();

		// spawn city explosin particle system
		Instantiate(explosion, transform.position + explosion.transform.position, explosion.transform.rotation);

		// play building explosion sound
		SoundManager.instance.PlaySingle (buildingExplosionSound);

		// place rubble pile
		GameObject obj = (GameObject) Instantiate(rubble, transform.position, transform.rotation);
		obj.transform.SetParent (transform.parent);

		// destroy gameobject
		Destroy(gameObject);
	}

	IEnumerator SpawnPeople () {
		People person;

		while (true) {
			// activate random person
			person = peopleManager.GetRandomPerson();
			person.Activate (transform.position);

			// wait to spawn new person
			yield return new WaitForSeconds (gameManager.RandomFloat(minPeopleSpawnTime, maxPeopleSpawnTime));
		}
	}
}
