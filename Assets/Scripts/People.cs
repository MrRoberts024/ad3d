﻿using UnityEngine;
using System.Collections;

public class People : MonoBehaviour {

	public GameObject collectEffect;
	public AudioClip collectSound;

	private float lifeTime = 10f;
	private Vector3 holdingPoint = new Vector3 (0f, 0f, -100f);
	private GameManager gameManager;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
	}

	public void PersonTouched() {
		// notify gamemanger to increase credits
		gameManager.PersonCollected();

		// spawn collection particle effect
		Instantiate(collectEffect, transform.position + collectEffect.transform.position, collectEffect.transform.rotation);

		// play collection sound
		SoundManager.instance.PlaySingle (collectSound, 0.2f);

		// deactivate this object
		Deactivate();
	}

	private void Expire() {
		// deactivate gameobject after lifetime expires if not already deactivated
		if (gameObject.activeSelf) {
			Deactivate();
		}
	}

	public void Deactivate() {
		// deactivate person
		gameObject.SetActive(false);

		// move to holding point
		transform.position = holdingPoint;

		// cancel invoke if it is active
		CancelInvoke();
	}

	public void Activate(Vector3 spawnPoint) {
		// move to spawn point
		transform.position = spawnPoint;

		// activate person
		gameObject.SetActive(true);

		// ensure person expires if not collected
		Invoke ("Expire", lifeTime);
	}
}
