﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartManager : MonoBehaviour {

	public GameObject optionsMenu;
	public GameObject shopMenu;
	public GameObject leaderboard;
	public Text credits;

	private GameManager gameManager;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
	}

	public void StartGame() {
		gameManager.LoadLevel ("02_Level_00");
	}

	public void OptionsMenu() {
		if (optionsMenu.activeSelf) {
			optionsMenu.SetActive (false);
		} else {
			optionsMenu.SetActive (true);
		}
	}

	public void ShopMenu() {
		if (shopMenu.activeSelf) {
			shopMenu.SetActive (false);
			// make sure credits total on start screen is still correct
			credits.text = PrefsManager.GetCredits ().ToString ();
		} else {
			shopMenu.SetActive (true);
		}
	}

	public void Leaderboard() {
		if (leaderboard.activeSelf) {
			leaderboard.SetActive (false);
		} else {
			leaderboard.SetActive (true);
		}
	}
}
