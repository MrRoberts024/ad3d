﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using UnityEngine.Advertisements;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour {

	public int bonusBasePoints = 50;
	private float adShowRate = 0.75f;

	private Text scoreText;
	private Text bonusText;
	private Text creditText;
	private Text totalCreditText;
	private Text creditHelpText;
	private static GameObject thisManager;

	private int citiesLeft;
	private int aaLeft;
	private int score;
	private int credits;
	private int multiplier;

	// statistics
	private int shotsFired;
	private int shotsHit;
	private int multiHits;
	private float timeElapsed;

	// gamesparks user id
	private string userId = "";

	// random number generator
	private System.Random random = new System.Random ();

	void Awake() {
		// singleton gamemanager
		if (thisManager == null) {
			thisManager = gameObject;
			DontDestroyOnLoad(gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		// set initial score here, first scene loaded isnt working right in OnLevelWasLoaded
		FindTextWithName("Score").text = PrefsManager.GetHighScore ().ToString();
		FindTextWithName("Credits").text = PrefsManager.GetCredits().ToString();

		// set sfx volume based on playerpref
		AudioListener.volume = PrefsManager.GetSFXVolume();

		// authenticate with gamesparks, use delay to ensure gamesparks initializes first
		Invoke("AuthenticateDevice", 1f);
	}

	void AuthenticateDevice() {
		new GameSparks.Api.Requests.DeviceAuthenticationRequest().Send((response) => {
			if (!response.HasErrors) {
				Debug.Log("Device Authenticated...");
				// store userId for use in leaderboard
				userId = response.UserId;

				// if display name is not set change it to default
				if (response.DisplayName == null) {
					new GameSparks.Api.Requests.ChangeUserDetailsRequest().SetDisplayName(PrefsManager.GetPlayerName()).Send((nameresponse) => {
						if (!nameresponse.HasErrors) {
							Debug.Log("Display name updated");
						} else {
							Debug.Log("Error updating display name");
						}
					});
				} else {
					Debug.Log("Display name: " + response.DisplayName);
				}
				// if new user post current saved high score
				if (response.NewPlayer == true) {
					Debug.Log("new player");
					new GameSparks.Api.Requests.LogEventRequest_postScore().Set_score(PrefsManager.GetHighScore()).Send((scoreresponse) => {
						if (scoreresponse.HasErrors) {
							Debug.Log("Failed posting score");
						} else {
							Debug.Log("Success posting score");
						}
					});
				}
			} else {
				Debug.Log("Error Authenticating Device...");
			}
		});
	}
		
	void OnLevelWasLoaded() {
		switch (SceneManager.GetActiveScene ().name) {
		case "01a_Start":
			// returned to main menu
			// display high score
			score = PrefsManager.GetHighScore ();
			scoreText = FindTextWithName ("Score");
			scoreText.text = score.ToString ();

			//display credits
			credits = PrefsManager.GetCredits ();
			creditText = FindTextWithName ("Credits");
			creditText.text = credits.ToString ();

			Time.timeScale = 1f;

			break;
		case "02_Level_00":
			// check if tutorial should be presented
			if (PrefsManager.isTutorialCompleted ()) {
				// reset timescale
				Time.timeScale = 1f;
			} else {
				// reset timescale and begin tutorial
				Time.timeScale = 0f;
			}

			// started a new game, reset all variables
			citiesLeft = 4;
			aaLeft = 1;
			score = 0;
			credits = 0;
			shotsFired = 0;
			shotsHit = 0;
			multiHits = 0;
			timeElapsed = 0;

			// set multiplier based on what is stored in player prefs
			multiplier = PrefsManager.GetMulitplier();

			// find and set score and bonus and credit text
			scoreText = FindTextWithName ("Score");
			scoreText.text = score.ToString ();
			creditText = FindTextWithName ("Credits");
			creditText.text = credits.ToString ();
			bonusText = FindTextWithName ("Bonus");
			break;
		case "03b_Lose":
			// end of game screen, find and set bonus text
			scoreText = FindTextWithName ("Score");
			scoreText.text = score.ToString ();
			creditText = FindTextWithName ("Credits");
			creditText.text = credits.ToString ();
			bonusText = FindTextWithName ("Bonus");

			// determine if new high score
			int oldHighScore = PrefsManager.GetHighScore ();
			if (score > oldHighScore) {
				// new high score, update prefs
				PrefsManager.SetHighScore (score);
				bonusText.text = "High Score!";
			} else {
				bonusText.text = "";
			}

			// submit score to gamesparks
			new GameSparks.Api.Requests.LogEventRequest_postScore ().Set_score (score).Send ((response) => {
				if (response.HasErrors) {
					Debug.Log ("Failed posting score");
					PrefsManager.SetPostScoreFailed ();
				} else {
					Debug.Log ("Success posting score");
				}
			});

			// update credits
			int newCredits = PrefsManager.GetCredits () + credits;
			PrefsManager.SetCredits (newCredits);

			// update total credits
			totalCreditText = FindTextWithName ("TotalCredits");
			totalCreditText.text = newCredits.ToString ();

			// Setup reference to storemanager and update store
			StoreManager storeManager = GameObject.FindObjectOfType<StoreManager> ();
			storeManager.UpdateStore ();

			// send analytics to unity
			Dictionary<string, object> customData = new Dictionary<string, object> () {
				{ "score",score },
				{ "credits",credits },
				{ "shotsFire",shotsFired },
				{ "shotsHit",shotsHit },
				{ "multiHit",multiHits },
				{ "timeElapsed",timeElapsed },
				{ "aaLeft",aaLeft },
				{ "citiesLeft", citiesLeft }
			};
			AnalyticsResult result = Analytics.CustomEvent ("EndGame", customData);
			Debug.Log (result);

			// show ad
			if (Advertisement.IsReady () && (adShowRate >= RandomFloat(0f,1.0f)) ) {
				Debug.Log ("show ad");
				Advertisement.Show ("video");
			} else {
				Debug.Log ("ad not ready");
			}

			break;
		default:
			Debug.LogError ("Unexpected Scene Name");
			break;
		}
	}

	private Text FindTextWithName (string name) {
		// find text object with given name
		Text[] textObjects = GameObject.FindObjectsOfType<Text> ();
		foreach (Text text in textObjects) {
			if (text.name == name) {
				return text;
			}
		}
		return null;
	}

	public void CityDestroyed() {
		citiesLeft--;
		if (citiesLeft <= 0) {
			// all cities destroyed end game
			EndGame();
		}
	}

	public void AADestroyed() {
		aaLeft--;
		if (aaLeft <= 0) {
			// all aa destroyed end game
			EndGame ();
		}
	}

	public void ShotFired() {
		shotsFired++;
	}

	public void ShotHit() {
		shotsHit++;
	}

	public void MultiHit() {
		multiHits++;
	}

	public void EndGame ()
	{
		// set elapsed time stat
		timeElapsed = Time.timeSinceLevelLoad;

		Time.timeScale = 0.25f;
		Invoke ("LoadLoseScreen", 0.50f);
	}

	public void LoadLoseScreen() {
		// game is over, load next level
		LoadLevel ("03b_Lose");
	}

	public void UpdateScore(int points) {
		score += (points * citiesLeft);
		scoreText.text = score.ToString ();
	}

	public void BonusHit (int hits) {
		// add bonus points to score
		UpdateScore (bonusBasePoints * hits * citiesLeft);

		// increment multihit for stats
		multiHits++;

		// update bonus text and setup crossfade of alpha channel
		bonusText.CrossFadeAlpha (1f, 0f, false);
		bonusText.text = hits + "X Hit Bonus!";
		bonusText.CrossFadeAlpha (0f, 2f, false);
	}

	public void PersonCollected () {
		// increment credits, 1 credit multiplied by the multiplier level
		credits += 2;  //(1 * multiplier);

		// update credit ui text
		creditText.text = credits.ToString ();
	}

	public void AdWatched () {
		// update credits
		int newCredits = PrefsManager.GetCredits () + credits;
		PrefsManager.SetCredits (newCredits);

		// update score text
		int newCreditsText = credits * 2;
		creditText.text = newCreditsText.ToString ();
	}

	public float RandomFloat ( float min, float max) {
		double rnd = random.NextDouble ();
		return (float)(rnd * (max - min) + min);
	}

	public int RandomNext ( int max ) {
		return random.Next (max);
	}
		
	public void LoadLevel(string name){
		SceneManager.LoadScene (name);
	}

	public bool showAdOption() {
		if (credits > 0) {
			return true;
		} else {
			return false;
		}
	}

	public string getUserId() {
		return userId;
	}
}
