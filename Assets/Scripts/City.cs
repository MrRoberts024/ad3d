﻿using UnityEngine;
using System.Collections;

public class City : MonoBehaviour {

	public GameObject[] shortBuildings;
	public GameObject[] tallBuildings;

	// random number generator
	private System.Random random = new System.Random ();

	private int firstTall = -1;
	private int firstShort = -1;

	// Use this for initialization
	void Start () {
		// assign random building prefabs to building slots
		// slot 1 and 4 are tall buildings, 2 and 3 are short buildings
		for (int count = 0; count < transform.childCount; count++) {
			switch (count) {
			case 0:
			case 3:
				RandomTallBuilding (transform.GetChild (count));
				break;
			case 1:
			case 2:
				RandomShortBuilding (transform.GetChild (count));
				break;
			default:
				Debug.LogError ("Building slot out of bounds");
				break;
			}
		}
	}

	private void RandomTallBuilding(Transform parent) {
		int index;
		do {
			index = random.Next (tallBuildings.Length);
		} while (index == firstTall);
		firstTall = index;
		GameObject obj = Instantiate (tallBuildings [index], parent.position, tallBuildings [index].transform.rotation) as GameObject;
		obj.transform.SetParent (parent);
		obj.transform.position = parent.transform.position + tallBuildings [index].transform.position;
	}

	private void RandomShortBuilding(Transform parent) {
		int index;
		do {
			index = random.Next (shortBuildings.Length);
		} while (index == firstShort);
		firstShort = index;
		GameObject obj = Instantiate (shortBuildings [index], parent.position, shortBuildings [index].transform.rotation) as GameObject;
		obj.transform.SetParent (parent);
	}
}
