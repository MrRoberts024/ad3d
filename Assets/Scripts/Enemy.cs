﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public int pointValue;
	public int damage;
	public GameObject groundExplosion;
	public AudioClip groundExplosionSound;
	public bool isEnemyProjectile;

	private GameManager gameManager;
	private Vector3 lastPosition;
	private Vector3 holdingPoint;

	void Start () {
		gameManager = GameObject.FindObjectOfType<GameManager> ();
		lastPosition = transform.position;
	}

	public void SetHoldingPosition (Vector3 point) {
		holdingPoint = point;
	}

	void OnCollisionEnter (Collision col) {
		// collision with ground
		if (col.gameObject.name == "Ground") {
			// spawn ground explosion particle system
			Instantiate(groundExplosion, transform.position + groundExplosion.transform.position, groundExplosion.transform.rotation);

			// play ground explosion audio
			SoundManager.instance.PlaySingle(groundExplosionSound, 0.5f);

			// Deactivate this object
			Deactivate();
		} else if (col.gameObject.name == gameObject.name) {
			// collision with same enemy type (likely on spawn)
			// deactivate to prevent odd spinning and collision behavior
			// destroy the enemy with a higher y position
			if (col.gameObject.transform.position.y < transform.position.y) {
				Deactivate();
			}
		}
	}

	void Update () {
		// if enemy moved off screen destroy it
		if (transform.position.x < -10 || transform.position.y < -10) {
			gameObject.SetActive(false);
		}
	}

	void FixedUpdate () {
		if (!isEnemyProjectile) {
			// align rocket rotation with movement vector
			Vector3 currentPosition = transform.position;
			Vector3 changeVector = currentPosition - lastPosition;
			if (changeVector.x != 0f && changeVector.y != 0f && transform.position.y < 46) {
				transform.rotation = Quaternion.LookRotation(changeVector);
			}

			lastPosition = currentPosition;
		}
	}

	void GoBoom () {
		// Update score
		gameManager.UpdateScore(pointValue);

		// deactivate this enemy
		Deactivate ();
	}

	void Deactivate () {
		if (isEnemyProjectile) {
			Destroy (gameObject);
		} else {
			// Deactivate this object, move to holding position
			gameObject.SetActive (false);
			gameObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			gameObject.transform.position = holdingPoint;
		}
	}
}
