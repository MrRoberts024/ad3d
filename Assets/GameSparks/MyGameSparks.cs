using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
	public class LogEventRequest_postScore : GSTypedRequest<LogEventRequest_postScore, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_postScore() : base("LogEventRequest"){
			request.AddString("eventKey", "postScore");
		}
		public LogEventRequest_postScore Set_score( long value )
		{
			request.AddNumber("score", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_postScore : GSTypedRequest<LogChallengeEventRequest_postScore, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_postScore() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "postScore");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_postScore SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_postScore Set_score( long value )
		{
			request.AddNumber("score", value);
			return this;
		}			
	}
	
}
	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_allTimeLeaderboard : GSTypedRequest<LeaderboardDataRequest_allTimeLeaderboard,LeaderboardDataResponse_allTimeLeaderboard>
	{
		public LeaderboardDataRequest_allTimeLeaderboard() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "allTimeLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_allTimeLeaderboard (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_allTimeLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_allTimeLeaderboard : GSTypedRequest<AroundMeLeaderboardRequest_allTimeLeaderboard,AroundMeLeaderboardResponse_allTimeLeaderboard>
	{
		public AroundMeLeaderboardRequest_allTimeLeaderboard() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "allTimeLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_allTimeLeaderboard (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_allTimeLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_allTimeLeaderboard : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_allTimeLeaderboard(GSData data) : base(data){}
		public long? score{
			get{return response.GetNumber("score");}
		}
	}
	
	public class LeaderboardDataResponse_allTimeLeaderboard : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_allTimeLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> Data_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> First_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> Last_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_allTimeLeaderboard : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_allTimeLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> Data_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> First_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_allTimeLeaderboard> Last_allTimeLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_allTimeLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_allTimeLeaderboard(data);});}
		}
	}
}	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_dailyLeaderboard : GSTypedRequest<LeaderboardDataRequest_dailyLeaderboard,LeaderboardDataResponse_dailyLeaderboard>
	{
		public LeaderboardDataRequest_dailyLeaderboard() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "dailyLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_dailyLeaderboard (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_dailyLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_dailyLeaderboard : GSTypedRequest<AroundMeLeaderboardRequest_dailyLeaderboard,AroundMeLeaderboardResponse_dailyLeaderboard>
	{
		public AroundMeLeaderboardRequest_dailyLeaderboard() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "dailyLeaderboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_dailyLeaderboard (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_dailyLeaderboard SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_dailyLeaderboard : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_dailyLeaderboard(GSData data) : base(data){}
		public long? score{
			get{return response.GetNumber("score");}
		}
	}
	
	public class LeaderboardDataResponse_dailyLeaderboard : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_dailyLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> Data_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> First_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> Last_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_dailyLeaderboard : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_dailyLeaderboard(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> Data_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> First_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_dailyLeaderboard> Last_dailyLeaderboard{
			get{return new GSEnumerable<_LeaderboardEntry_dailyLeaderboard>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_dailyLeaderboard(data);});}
		}
	}
}	

namespace GameSparks.Api.Messages {


}
